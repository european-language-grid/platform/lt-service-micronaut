/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import eu.elg.model.Request;
import eu.elg.model.Response;
import eu.elg.model.ResponseMessage;
import eu.elg.model.StatusMessage;
import io.micronaut.core.async.annotation.SingleResult;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.multipart.CompletedFileUpload;
import io.micronaut.http.multipart.StreamingFileUpload;
import io.micronaut.http.sse.Event;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * <p>Base class for a Micronaut controller that implements the ELG internal LT service API for binary-content requests
 * (e.g. audio or images).</p>
 *
 * <h2>Basic usage</h2>
 * <p>Subclass this class and override the appropriate <code>handle</code> or <code>handleSync</code> method.  Your
 * subclass must be annotated as a Micronaut {@link Controller} with the URL path at which you want it to respond
 * (typically <code>/process</code>).</p>
 * <p>Implement {@link #handleSync handleSync} to handle the request in a typical blocking
 * fashion (returning a {@link Response} directly), or {@link #handle handle} to use
 * non-blocking reactive streams.</p>
 * <pre><code>
 * &#x40;Controller("/process")
 * public class MyService extends BinaryLTService&lt;AudioRequest, StreamingFileUpload, LTService.Context&gt; {
 *
 *   private ReactorHttpClient client;
 *
 *   private AppConfig conf;
 *
 *   public MyService(&#x40;Client ReactorHttpClient client, AppConfig conf) {
 *     this.client = client;
 *     this.conf = conf;
 *   }
 *
 *   protected Mono&lt;TextsResponse&gt; handle(AudioRequest req, StreamingFileUpload content, LTService.Context ctx) {
 *     // StreamingFileUpload is a Publisher&lt;PartData&gt;
 *     Flux&lt;ByteBuffer&gt; body = Flux.from(content).map(PartData::getByteBuffer);
 *     return client.retrieve(HttpRequest.POST(conf.getEndpoint(), body), MyResponse.class)
 *                  .last().map(this::makeELGResponse);
 *   }
 *
 *   private TextsResponse makeELGResponse(MyResponse r) {
 *     // ...
 *   }
 * }
 * </code></pre>
 * <p>Any exceptions thrown by <code>handleSync</code> or returned by a failing publisher from <code>handle</code> will
 * be returned to the caller as proper ELG failure responses.  The {@link ELGException} type allows you to fully
 * customise the error response, any other exception will be returned as an <code>elg.service.internalError</code>.</p>
 *
 * <h2>Progress reporting</h2>
 * <p>The ELG API specification allows services to send progress updates back to the caller during
 * request processing, using server-sent events.  To send a progress report you can use the
 * <code>reportProgress</code> method on the {@link Context} which is passed to each handler
 * method.  For example:</p>
 * <pre><code>ctx.reportProgress(20.0);</code></pre>
 * <p>The {@link LTService.Context#reportProgress(Double) ctx.reportProgress} method takes a percentage, which should be
 * between 0 and 100, and an optional {@link StatusMessage} to provide more information to the caller.</p>
 *
 * <h2>Advanced context usage</h2>
 * <p>It is possible to subclass the Context type in order to bind path variables from your controller URI template.
 * See {@link LTService} for full details of this mechanism.</p>
 *
 * @param <T> the type of ELG request that this controller handles.  If you want to handle multiple request types in the
 *            same controller then specify {@link Request} and do your own casting in the handler.
 * @param <D> the data type in which the binary content will be passed to your handler methods.  This should be one of
 *            three basic types: {@link StreamingFileUpload} to stream the content direct from the incoming request,
 *            {@link CompletedFileUpload} to have Micronaut buffer the content (in memory or on disk), or
 *            <code>byte[]</code> to gather the data into a single in-memory byte array.  If using
 *            {@link StreamingFileUpload} you must implement the reactive {@link #handle handle}
 *            method, {@link #handleSync handleSync} will not work for streaming uploads.
 * @param <C> context class passed to your handler methods.  In most cases this will simply be {@link LTService.Context}
 *            but you can create a subclass to bind path variables
 */
public class BinaryLTService<T extends Request<T>, D, C extends LTService.Context> extends LTService<T, C> {

  /**
   * Controller action that accepts a multipart request message (audio or image request) and returns a single JSON
   * response or failure message.  Any progress updates generated by the handler are discarded.
   *
   * @param request the request JSON parsed from the request body part
   * @param content binary content of the audio or image
   * @param ctx     a context for this request, which is created by Micronaut's {@link RequestBean} mechanism to allow
   *                for binding things like path variables.
   * @return the response or failure message produced by the handler
   */
  @Post
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces({MediaType.APPLICATION_JSON, MediaType.ALL})
  public Mono<HttpResponse<ResponseMessage>> multipartRequestSingleResponse(@JsonField("request") T request,
                                                                            @Part("content") D content,
                                                                            @RequestBean C ctx) {
    return sendSingleResponse(multipartRequest(request, content, ctx));
  }

  /**
   * Controller action that accepts a multipart request message (audio or image request) and returns a stream of
   * server-sent events consisting of zero or more progress messages followed by one JSON response or failure message.
   *
   * @param request the request JSON parsed from the request body
   * @param content binary content of the audio or image
   * @param ctx     a context for this request, which is created by Micronaut's {@link RequestBean} mechanism to allow
   *                for binding things like path variables.
   * @return event stream of progress messages terminated by the final handler response
   */
  @Post
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.TEXT_EVENT_STREAM)
  public Flux<Event<ResponseMessage>> multipartRequestStreamingResponse(@JsonField("request") T request,
                                                                        @Part("content") D content,
                                                                        @RequestBean C ctx) {
    return sendProgress(() -> multipartRequest(request, content, ctx), ctx);
  }

  private Mono<ResponseMessage> multipartRequest(T request, D content, C ctx) {
    return wrap(request, handle(request, content, ctx));
  }

  /**
   * <p>Main entry point method that should be overridden in subclasses to handle binary requests (audio or image) in a
   * non-blocking reactive manner.  Note that this method should not throw exceptions directly, rather it should fail by
   * returning a publisher that terminates with an error.</p>
   * <p>The default implementation delegates to {@link #handleSync handleSync} on the {@link #syncScheduler()},
   * <em>unless</em> the content type is {@link StreamingFileUpload} in which case it returns a publisher that
   * immediately fails with an unsupported operation exception (streaming uploads must be handled reactively).</p>
   *
   * @param request the parsed JSON request message
   * @param content the binary content
   * @param ctx     context that can be used to issue partial progress updates
   * @return a single-result publisher that yields the final response
   */
  @SingleResult
  protected Publisher<? extends Response<?>> handle(T request, D content, C ctx) {
    if(content instanceof StreamingFileUpload) {
      return Mono.error(new UnsupportedOperationException("not implemented"));
    } else {
      return Mono.fromCallable(() -> handleSync(request, content, ctx)).subscribeOn(syncScheduler());
    }
  }

  /**
   * <p>Main entry point method that should be overridden in subclasses to handle binary requests (audio or image) in a
   * synchronous (possibly blocking) manner.  Any exceptions thrown will be returned to the caller as a failure
   * message</p>
   * <p>Note that in order to handle binary requests in this way the type parameter <code>D</code> must be set to a
   * type that Micronaut can bind with the binary data in a single complete block, typically either <code>byte[]</code>
   * or {@link io.micronaut.http.multipart.CompletedFileUpload}.  If you are able to process the data reactively (by
   * setting the type as {@link StreamingFileUpload} and using the non-blocking {@link #handle handle} method) this
   * will likely be more efficient as the binary content does not need to be fully buffered by Micronaut before
   * processing.</p>
   *
   * @param request the parsed JSON request message
   * @param content the binary content
   * @param ctx     context that can be used to issue partial progress updates
   * @return a successful response - in case of failure throw an exception instead
   */
  protected Response<?> handleSync(T request, D content, C ctx) {
    throw new UnsupportedOperationException("not implemented");
  }
}
