/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import eu.elg.model.Request;
import eu.elg.model.StandardMessages;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Requires;
import io.micronaut.core.bind.exceptions.UnsatisfiedArgumentException;
import io.micronaut.core.convert.exceptions.ConversionErrorException;
import io.micronaut.http.exceptions.ContentLengthExceededException;
import io.micronaut.web.router.exceptions.DuplicateRouteException;
import io.micronaut.web.router.exceptions.UnsatisfiedRouteException;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Default set of exception mappers to map common Micronaut exceptions into ELG failure messages.
 */
@Requires(property = "elg.error-handlers", value = "true", defaultValue = "true")
@Factory
public class DefaultELGExceptionMappers {

  @Bean
  public ELGExceptionMapper<ContentLengthExceededException> contentLengthExceeded() {
    return new ELGExceptionMapper<>(ContentLengthExceededException.class,
            (t) -> StandardMessages.elgRequestTooLarge());
  }

  @Bean
  public ELGExceptionMapper<DuplicateRouteException> duplicateRouteException() {
    return new ELGExceptionMapper<>(DuplicateRouteException.class,
            (t) -> StandardMessages.elgRequestInvalid().withDetail(
                    new LinkedHashMap<>(Collections.singletonMap(
                            "mutipleRoutes", t.getUriRoutes().stream()
                                    .map(Objects::toString).collect(Collectors.toList())))));
  }

  @Bean
  public ELGExceptionMapper<UnsatisfiedRouteException> unsatisfiedRouteException() {
    return new ELGExceptionMapper<>(UnsatisfiedRouteException.class,
            (t) -> StandardMessages.elgRequestInvalid().withDetail(
                    new LinkedHashMap<>(Collections.singletonMap(
                            "argument", t.getArgument().toString()))));
  }

  @Bean
  public ELGExceptionMapper<UnsatisfiedArgumentException> unsatisfiedArgumentException() {
    return new ELGExceptionMapper<>(UnsatisfiedArgumentException.class,
            (t) -> StandardMessages.elgRequestInvalid().withDetail(
                    new LinkedHashMap<>(Collections.singletonMap(
                            "argument", t.getArgument().toString()))));
  }

  @Bean
  public ELGExceptionMapper<ConversionErrorException> conversionErrorException() {
    return new ELGExceptionMapper<>(ConversionErrorException.class, (t) -> {
      if(Request.class.isAssignableFrom(t.getArgument().getType()) &&
              t.getConversionError().getCause() instanceof InvalidTypeIdException) {
        // special case - this is where a controller expects one Request subtype but the caller passed another (e.g.
        // controller accepts text but caller passed structuredText)
        return StandardMessages.elgRequestTypeUnsupported(
                ((InvalidTypeIdException) t.getConversionError().getCause()).getTypeId());
      }
      LinkedHashMap<String, Object> detail = new LinkedHashMap<>();
      detail.put("argument", t.getArgument().toString());
      detail.put("conversionError", t.getMessage());
      return StandardMessages.elgRequestInvalid().withDetail(detail);
    });
  }

  @Bean
  public ELGExceptionMapper<JsonProcessingException> jsonProcessingException() {
    return new ELGExceptionMapper<>(JsonProcessingException.class,
            (t) -> StandardMessages.elgRequestInvalid().withDetail(
                    new LinkedHashMap<>(Collections.singletonMap(
                            "reason", t.getMessage()))));
  }

  @Bean
  public ELGExceptionMapper<ConstraintViolationException> constraintViolationException() {
    return new ELGExceptionMapper<>(ConstraintViolationException.class,
            (t) -> StandardMessages.elgRequestInvalid().withDetail(
                    new LinkedHashMap<>(Collections.singletonMap(
                            "violations", t.getConstraintViolations().stream().map(Object::toString)
                                    .collect(Collectors.toList())))));
  }

  @Bean
  public ELGExceptionMapper<Throwable> generalException() {
    return new ELGExceptionMapper<>(Throwable.class, (t) -> StandardMessages.elgServiceInternalError(t.getMessage()));
  }
}
