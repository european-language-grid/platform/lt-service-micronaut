/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.elg.model.*;
import io.micronaut.context.annotation.Value;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.ReflectiveAccess;
import io.micronaut.core.async.annotation.SingleResult;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.annotation.*;
import io.micronaut.http.sse.Event;
import org.reactivestreams.Publisher;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.Collections;
import java.util.function.Supplier;

/**
 * <p>Base class for a Micronaut controller that implements the ELG internal LT service API for text-based requests.
 * To handle requests with binary content (audio or images) extend {@link BinaryLTService} instead of this class.</p>
 *
 * <h2>Basic usage</h2>
 * <p>Subclass this class and override the appropriate <code>handle</code> or <code>handleSync</code> method.  Your
 * subclass must be annotated as a Micronaut {@link Controller} with the URL path at which you want it to respond
 * (typically <code>/process</code>).</p>
 * <p>Implement {@link #handleSync handleSync} to handle the request in a typical blocking fashion (returning a
 * {@link Response} directly), or {@link #handle handle} to use non-blocking reactive streams.</p>
 * <pre><code>
 * &#x40;Controller("/process")
 * public class MyService extends LTService&lt;TextRequest, LTService.Context&gt; {
 *
 *   protected AnnotationsResponse handleSync(TextRequest req, LTService.Context ctx) throws ELGException {
 *     if(req.getContent().size() &gt; 10) {
 *       return new AnnotationsResponse().withFeature("length", req.getContent().size());
 *     } else {
 *       throw new ELGException(StandardMessages.elgRequestInvalid());
 *     }
 *   }
 * }
 * </code></pre>
 * <p>Any exceptions thrown by {@link #handleSync handleSync} will be returned to the caller as proper ELG failure
 * responses.  The {@link ELGException} type allows you to fully customise the error response, any other exception will
 * be returned as an <code>elg.service.internalError</code>.</p>
 *
 * <h2>Progress reporting</h2>
 * <p>The ELG API specification allows services to send progress updates back to the caller during
 * request processing, using server-sent events.  To send a progress report you can use the
 * <code>reportProgress</code> method on the {@link Context} which is passed to each handler
 * method.  For example:</p>
 * <pre><code>ctx.reportProgress(20.0);</code></pre>
 * <p>The {@link LTService.Context#reportProgress(Double) ctx.reportProgress} method takes a percentage, which should
 * be between 0 and 100, and an optional {@link StatusMessage} to provide more information to the caller.</p>
 *
 * <h2>Advanced context usage</h2>
 * <p>Since the <code>&#x40;Post</code> annotated controller endpoint methods are provided by
 * the LTService superclass rather than being implemented directly in your
 * <code>&#x40;Controller</code>, it is not possible to add extra parameters to your handler methods
 * to bind things like <code>&#x40;PathVariable</code> values from the controller URI template.  If you need to access
 * these then you can do so by <em>subclassing</em> {@link Context} and passing your subclass as the second generic type
 * parameter instead of just using
 * <code>LTService.Context</code>.  On your subclass you can then add Java Bean properties or
 * constructor arguments annotated for binding by the Micronaut <code>&#x40;RequestBean</code> mechanism.  Most commonly
 * this means <code>&#x40;PathVariable</code> properties, since the ELG platform does not pass query parameters (your
 * service parameters come via the JSON {@link Request#getParams}).  The following example shows constructor
 * binding:</p>
 * <pre><code>
 * &#x40;Controller("/translate/{from}/{to}")
 * public class TranslationService extends LTService&lt;TextRequest, TranslationService.Context&gt; {
 *
 *   // "Introspected" annotation is required for data binding to work
 *   &#x40;Introspected
 *   public static class Context extends LTService.Context {
 *     public final String from;
 *     public final String to;
 *
 *     // PathVariable annotations bind the {} expressions in the controller URI template
 *     public Context(&#x40;PathVariable("from") String from,
 *                    &#x40;PathVariable("to") String to) {
 *       this.from = from;
 *       this.to = to;
 *     }
 *   }
 *
 *   // Bean that does the actual work
 *   private Translator translator;
 *
 *   // Dependency injection via constructor arguments
 *   public TranslationService(Translator translator) {
 *     this.translator = translator;
 *   }
 *
 *   protected AnnotationsResponse handleSync(TextRequest req, TranslationService.Context ctx) throws ELGException {
 *     return translator.translate(ctx.from, ctx.to, req.getContent());
 *   }
 * }
 * </code></pre>
 * <p>Instead of using constructor arguments it is possible to apply the <code>&#x40;PathVariable</code>
 * annotations to fields directly, but the fields <em>must</em> have proper getter and setter methods in order to be
 * eligible for data binding - <a href="https://projectlombok.org">Lombok</a>
 * <code>&#x40;Getter</code> and <code>&#x40;Setter</code> annotations can help with this.</p>
 *
 * @param <T> the type of ELG request that this controller handles.  If you want to handle multiple request types in the
 *            same controller then specify {@link Request} and do your own casting in the handler.
 * @param <C> context class passed to your handler methods.  In most cases this will simply be {@link LTService.Context}
 *            but you can create a subclass to bind path variables
 */
public class LTService<T extends Request<T>, C extends LTService.Context> {

  /**
   * <p>Maximum interval between successive progress messages - if no progress has been reported for this length of
   * time, the most recent progress event is repeated in order to ensure the connection remains alive.</p>
   * <p>Configurable via the <code>elg.progress-interval</code> configuration property.</p>
   */
  @ReflectiveAccess
  @Value("${elg.progress-interval:15s}")
  protected Duration progressInterval;

  /**
   * Controller action that accepts a JSON request message (text or structuredText request) and returns a single JSON
   * response or failure message.  Any progress updates generated by the handler are discarded.
   *
   * @param request the request JSON parsed from the request body
   * @param ctx     a context for this request, which is created by Micronaut's {@link RequestBean} mechanism to allow
   *                for binding things like path variables.
   * @return the response or failure message produced by the handler
   */
  @Post
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces({MediaType.APPLICATION_JSON, MediaType.ALL})
  public Mono<HttpResponse<ResponseMessage>> jsonRequestSingleResponse(@Body T request,
                                                                       @RequestBean C ctx) {
    return sendSingleResponse(jsonRequest(request, ctx));
  }

  /**
   * Controller action that accepts a JSON request message (text or structuredText request) and returns a stream of
   * server-sent events consisting of zero or more progress messages followed by one JSON response or failure message.
   *
   * @param request the request JSON parsed from the request body
   * @param ctx     a context for this request, which is created by Micronaut's {@link RequestBean} mechanism to allow
   *                for binding things like path variables.
   * @return event stream of progress messages terminated by the final handler response
   */
  @Post
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_EVENT_STREAM)
  public Flux<Event<ResponseMessage>> jsonRequestStreamingResponse(@Body T request,
                                                                   @RequestBean C ctx) {
    return sendProgress(() -> jsonRequest(request, ctx), ctx);
  }

  private Mono<ResponseMessage> jsonRequest(T request, C ctx) {
    return wrap(request, handle(request, ctx));
  }

  /**
   * Error handler that renders 404 responses as an elg.service.not.found failure response.
   *
   * @param request the HTTP request
   * @return a "service not found" failure response
   */
  @Error(status = HttpStatus.NOT_FOUND)
  public HttpResponse<ResponseMessage> notFound(HttpRequest<?> request) {
    return HttpResponse.notFound(new Failure().withErrors(
            StandardMessages.elgServiceNotFound(request.getPath())).asMessage());
  }

  /**
   * Error handler that converts request JSON parsing errors into a standard ELG "request invalid" failure response.
   *
   * @param request        the incoming HTTP request
   * @param parseException the parsing error
   * @return an "elg.request.invalid" failure response
   */
  @Error
  public HttpResponse<ResponseMessage> errorHandler(HttpRequest<?> request, JsonProcessingException parseException) {
    return HttpResponse.badRequest(new Failure().withErrors(
                    StandardMessages.elgRequestInvalid()
                            .withDetail(Collections.singletonMap("reason", parseException.getMessage())))
            .asMessage());
  }


  /**
   * <p>Main entry point method that should be overridden in subclasses to handle text or structuredText requests in a
   * non-blocking reactive manner.  Note that this method should not throw exceptions directly, rather it should fail by
   * returning a publisher that terminates with an error.</p>
   * <p>The default implementation calls {@link #handleSync handleSync} on the {@link #syncScheduler()}.</p>
   *
   * @param request the parsed JSON request message
   * @param ctx     context that can be used to issue partial progress updates
   * @return a single-result publisher that yields the final response
   */
  @SingleResult
  protected Publisher<? extends Response<?>> handle(T request, C ctx) {
    return Mono.fromCallable(() -> handleSync(request, ctx)).subscribeOn(syncScheduler());
  }

  /**
   * <p>Main entry point method that should be overridden in subclasses to handle text or structuredText requests in a
   * synchronous (possibly blocking) manner.  Any exceptions thrown will be returned to the caller as a failure
   * message</p>
   *
   * @param request the parsed JSON request message
   * @param ctx     context that can be used to issue partial progress updates
   * @return a successful response - in case of failure throw an exception instead
   * @throws Exception if processing fails for any reason.  Any exception thrown will be returned to the user as
   * a valid ELG failure message.
   */
  protected Response<?> handleSync(T request, C ctx) throws Exception {
    throw new UnsupportedOperationException("not implemented");
  }

  /**
   * Given a mono that <em>either</em> publishes one successful response messages <em>or</em> fails with an exception,
   * convert the successful response to a 200 HttpResponse or map the exception to a failure response in ELG format.
   *
   * @param responseMessageMono publisher that either completes successfully with a "response" message or fails with an
   *                            exception
   * @return publisher that emits a suitable HTTP response, whether successful or failed, in ELG format.
   */
  protected Mono<HttpResponse<ResponseMessage>> sendSingleResponse(Mono<ResponseMessage> responseMessageMono) {
    return responseMessageMono.map(HttpResponse::ok)
            .onErrorResume(ELGException.class, e -> Mono.just(HttpResponse.status(
                    e.getStatus()).body((ResponseMessage) e.getBody().get())))
            .onErrorResume(t -> Mono.just(HttpResponse.serverError(
                    ELGErrorResponseProcessor.errorResponseForException(t,
                            LoggerFactory.getLogger(this.getClass()).isTraceEnabled()))))
            // respond in JSON regardless of the Accept header
            .map(r -> r.contentType(MediaType.APPLICATION_JSON_TYPE));
  }

  /**
   * Given a mono that <em>either</em> publishes one successful response messages <em>or</em> fails with an exception,
   * and uses a context to possibly emit progress updates while doing so, generate a flux that emits the stream of
   * progress updates plus the final response or failure message as server-sent events.
   *
   * @param responseMonoGenerator supplier that generates the Mono that will yield the final response
   * @param ctx                   context that the Mono may be using to publish progress updates
   * @return combined Flux of SSEs yielding the progress updates followed by the final response
   */
  protected Flux<Event<ResponseMessage>> sendProgress(Supplier<Mono<ResponseMessage>> responseMonoGenerator, C ctx) {
    ctx.progressEnabled = true;
    Flux<ResponseMessage> progressFlux = ctx.progressFlux(progressInterval);
    return progressFlux.mergeWith(
            Mono.defer(responseMonoGenerator)
                    .onErrorResume(ELGException.class, e -> Mono.just((ResponseMessage) e.getBody().get()))
                    .onErrorResume(t ->
                            Mono.just(ELGErrorResponseProcessor.errorResponseForException(t,
                                    LoggerFactory.getLogger(this.getClass()).isTraceEnabled())))
    ).handle((msg, sink) -> {
      sink.next(Event.of(msg));
      if(msg.getFailure() != null || msg.getResponse() != null) {
        // stop when we get to the "final" message (Response or Failure)
        sink.complete();
      }
    });
  }

  /**
   * The scheduler used to run {@link #handleSync}.
   *
   * @return {@link Schedulers#boundedElastic()} by default but this can be
   * overridden in subclasses if required.
   */
  protected Scheduler syncScheduler() {
    return Schedulers.boundedElastic();
  }

  /**
   * Wrap a {@link Response} in a proper message envelope, and augment it with any warnings that were generated during
   * parsing of the original request.
   *
   * @param request           the original request that triggered this handler
   * @param responsePublisher publisher that generates a single {@link Response}
   * @return the wrapped {@link ResponseMessage}
   */
  protected Mono<ResponseMessage> wrap(T request, Publisher<? extends Response<?>> responsePublisher) {
    return Mono.from(responsePublisher).map(resp -> {
      ResponseMessage msg = resp.asMessage();
      msg.addWarnings(request.unknownPropertyWarnings());
      return msg;
    });
  }

  /**
   * Context object passed to handler methods to provide hooks for progress reporting.  It is also possible to subclass
   * this class to bind path variables, see the main {@link LTService} docs for details.
   */
  @Introspected
  public static class Context {
    // this looks complex but what we're doing is producing a Flux of progress messages which
    // - fires an event whenever the ProgressReporter is called
    // - if no events have been published within progressInterval duration, re-emit the last message as a keepalive
    Flux<ResponseMessage> progressFlux(Duration progressInterval) {
      return Flux.concat(Mono.just(new Progress().withPercent(0d).asMessage()),
                      Flux.<Progress>create(s -> sink = s, FluxSink.OverflowStrategy.LATEST)
                              .map(Progress::asMessage))
              .switchMap((msg) -> Flux.interval(Duration.ZERO, progressInterval).map((i) -> msg));
    }

    private volatile FluxSink<Progress> sink;

    boolean progressEnabled = false;

    /**
     * Is progress reporting enabled for the current call?  If we are not sending progress reports to the caller
     * (because they don't accept the SSE response type) then {@link #reportProgress reportProgress} is a no-op.  In
     * this case services may wish to avoid entirely the overhead of generating progress reports.
     *
     * @return true if this request is sending progress reports back to the caller via SSE, false if not.
     */
    public boolean sendingProgress() {
      return progressEnabled;
    }

    /**
     * Report a partial progress message to the caller of this service, with a given completion percentage and
     * supplementary information message.  Note this is a no-op if the caller cannot accept SSE responses, if your
     * progress reports are expensive to generate then you may wish to check for this in advance by calling {@link
     * #sendingProgress()}
     *
     * @param percent percentage progress between 0.0 and 100.0
     * @param message supplementary information to pass back to the caller
     */
    public void reportProgress(Double percent, StatusMessage message) {
      if(sink != null && !sink.isCancelled()) {
        sink.next(new Progress().withPercent(percent).withMessage(message));
      }
    }

    /**
     * Report a partial progress message without any supplementary information.
     *
     * @param percent percentage progress between 0.0 and 100.0
     * @see #reportProgress(Double, StatusMessage)
     */
    public void reportProgress(Double percent) {
      reportProgress(percent, null);
    }
  }

}
