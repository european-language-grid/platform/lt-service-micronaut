/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import io.micronaut.context.annotation.Requires;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

/**
 * A trivial controller that simply responds with JSON <code>{"alive":true}</code>, for use as a basic liveness check.
 * This is a convenience for if you do not wish to depend on <code>micronaut-management</code> in order to use its
 * health check system.  The liveness controller can be disabled by setting <code>elg.liveness.enabled=false</code> or
 * moved to a different URI path from the default <code>/elg-alive</code> by setting
 * <code>elg.liveness.endpoint</code>.
 */
@Controller
@Requires(property = "elg.liveness.enabled", value = "true", defaultValue = "true")
public class LivenessController {

  @Get("${elg.liveness.endpoint:/elg-alive}")
  public HttpResponse<AliveResponse> alive() {
    return HttpResponse.ok(new AliveResponse());
  }

  @Introspected
  public static class AliveResponse {
    public boolean isAlive() {
      return true;
    }
  }
}
