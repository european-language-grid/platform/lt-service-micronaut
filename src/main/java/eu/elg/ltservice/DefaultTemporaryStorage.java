/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import eu.elg.model.ResponseMessage;
import eu.elg.model.StandardMessages;
import eu.elg.model.StatusMessage;
import eu.elg.model.responses.StoredResponse;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

/**
 * Default implementation of the temporary storage helper, using the standard Micronaut {@link HttpClient}.  By default
 * it will call the storage service at <code>http://storage.elg/store</code> but this can be overridden (for example for
 * local testing) via the configuration property <code>elg.temporary-storage.url</code>.
 */
@Singleton
@Requires(beans = HttpClient.class)
public class DefaultTemporaryStorage implements ReactorTemporaryStorage {

  private static final Logger log = LoggerFactory.getLogger(DefaultTemporaryStorage.class);

  private final HttpClient httpClient;

  private final URI endpoint;

  public DefaultTemporaryStorage(@Client HttpClient client,
                                 @Value("${elg.temporary-storage.url:http://storage.elg/store}") URI endpoint) {
    this.httpClient = client;
    this.endpoint = endpoint;
  }

  @Override
  public Mono<URI> store(Object data, MediaType mimeType, Duration ttl) {
    return Mono.from(httpClient.exchange(HttpRequest.POST(endpoint, data).uri(builder -> {
      if(ttl != null) {
        builder.queryParam("ttl", ttl.getSeconds());
      }
    }).contentType(Optional.ofNullable(mimeType).orElse(
            MediaType.APPLICATION_OCTET_STREAM_TYPE)), ResponseMessage.class)).flatMap(response -> {
      ResponseMessage m = response.body();
      if(m == null) {
        log.error("No response from temporary storage service");
        return Mono.error(new ELGException(StandardMessages.elgResponseInvalid()));
      }
      if(m.getResponse() instanceof StoredResponse) {
        return Mono.just(((StoredResponse) m.getResponse()).getUri());
      } else if(m.getFailure() != null && m.getFailure().getErrors() != null) {
        List<StatusMessage> messagesList = m.getFailure().getErrors();
        return Mono.error(new ELGException(response.getStatus(),
                messagesList.toArray(new StatusMessage[messagesList.size()])));
      } else {
        log.error("Unknown error from temporary storage service");
        return Mono.error(new ELGException(StandardMessages.elgServiceInternalError("storage service error")));
      }
    });
  }

  @Override
  public URI storeSync(Object data, MediaType mimeType, Duration ttl) throws ELGException {
    return store(data, mimeType, ttl).block();
  }
}
