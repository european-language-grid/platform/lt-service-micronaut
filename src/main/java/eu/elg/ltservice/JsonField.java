/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import io.micronaut.context.annotation.AliasFor;
import io.micronaut.core.bind.annotation.Bindable;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marker annotation for controller arguments that come from multipart "form fields" (i.e. parts <em>without</em> a
 * <code>filename</code> in their <code>Content-Disposition</code>).  Normally Micronaut can only bind "file uploads"
 * (parts <em>with</em> a <code>filename</code>) to POJOs, and "form fields" only to simple types like strings; this
 * annotation causes a second pass on the string to parse it as JSON into a POJO.
 */
@Target({FIELD, PARAMETER, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Bindable
public @interface JsonField {

  /**
   * The form field name to which we should bind.
   *
   * @return the form field name.
   */
  @AliasFor(annotation = Bindable.class, member = "value")
  String value() default "";
}
