/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import eu.elg.model.Failure;
import eu.elg.model.ResponseMessage;
import eu.elg.model.StandardMessages;
import eu.elg.model.StatusMessage;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.server.exceptions.response.Error;
import io.micronaut.http.server.exceptions.response.ErrorContext;
import io.micronaut.http.server.exceptions.response.ErrorResponseProcessor;
import jakarta.inject.Singleton;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Replacement for the standard Hateoas error formatter that ensures that even errors external to the ELG LT service
 * controllers will be returned to the caller as well-formed ELG error messages.  This behaviour may be disabled via the
 * configuration property <code>elg.error-handlers=false</code>, in which case only errors occurring <em>within</em> the
 * {@link LTService} controllers will be mapped to ELG format.  Note that in that mode exceptions that occur prior to
 * the point where Micronaut has decided which controller to hand off to (e.g. requests to a typo URI such as
 * <code>/procss</code> where the mapping should be <code>/process</code>) will not be converted to ELG format.
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Requires(property = "elg.error-handlers", value = "true", defaultValue = "true")
@Singleton
public class ELGErrorResponseProcessor implements ErrorResponseProcessor<ResponseMessage> {

  private final List<ELGExceptionMapper> mappers;

  public ELGErrorResponseProcessor(List<ELGExceptionMapper> mappers) {
    this.mappers = new ArrayList<>(mappers);
    // this is a topological sort that ensures that the handlers are ordered such that any handler for type X comes
    // before handlers for any of its superclasses, i.e. if we do a linear search through this list for a handler
    // able to handle a given exception we will find the most specific match first.
    this.mappers.sort((ELGExceptionMapper m1, ELGExceptionMapper m2) -> {
      if(m1.getType() == m2.getType()) {
        // both the same class
        return 0;
      } else if(m1.getType().isAssignableFrom(m2.getType())) {
        // m2 is more specific than m1
        return 1;
      } else if(m2.getType().isAssignableFrom(m1.getType())) {
        // m1 is more specific than m2
        return -1;
      } else {
        // neither is a subclass of the other, so defer to sorting names alphabetically
        // to make this a total ordering
        return m1.getType().getName().compareTo(m2.getType().getName());
      }
    });
  }

  @Override
  public MutableHttpResponse<ResponseMessage> processResponse(ErrorContext errorContext, MutableHttpResponse<?> response) {
    List<StatusMessage> errors = new ArrayList<>();
    if(errorContext.getRootCause().isPresent()) {
      // error caused by an exception, map the exception to a StatusMessage
      Throwable t = errorContext.getRootCause().get();
      // the get on the next line is safe as we know we have a mapper registerd for Throwable
      ELGExceptionMapper mapper = mappers.stream().filter(m -> m.getType().isAssignableFrom(t.getClass())).findFirst().get();
      errors.add(mapper.toStatusMessage(t));
    } else if(response.getStatus().equals(HttpStatus.NOT_FOUND)) {
      // special case for 404
      errors.add(StandardMessages.elgServiceNotFound(errorContext.getRequest().getPath()));
    } else {
      // generate an "internal error" response from the error data.
      if(!errorContext.hasErrors()) {
        errors.add(StandardMessages.elgServiceInternalError(response.status().getReason()));
      } else {
        for(Error err : errorContext.getErrors()) {
          errors.add(StandardMessages.elgServiceInternalError(err.getMessage()));
        }
      }
    }

    return response.body(new Failure().withErrors(errors).asMessage());
  }


  /**
   * Generate a failure message from a Java error or exception.
   *
   * @param throwable         the error or exception.  If this is an {@link ELGException} then its status messages will
   *                          be used in the failure message.  Any other throwable will be reported as a standard
   *                          "internal error" message, with stack trace information added if requested.
   * @param includeStacktrace should we include the stack trace information in a generated "internal error" message?
   * @return a {@link ResponseMessage} wrapping a {@link Failure}.
   */
  public static ResponseMessage errorResponseForException(Throwable throwable, boolean includeStacktrace) {
    if(throwable instanceof InvocationTargetException) {
      // unwrap
      throwable = throwable.getCause();
    }
    if(throwable instanceof ELGException) {
      Optional<Object> body = ((ELGException) throwable).getBody();
      if(body.isPresent()) { // this will always be the case, but compiler doesn't know that
        return (ResponseMessage) body.get();
      }
    }

    // either the exception isn't a HandlerException, or it is but the status is null
    StatusMessage statusMessage = StandardMessages.elgServiceInternalError(throwable.getMessage());
    if(includeStacktrace) {
      statusMessage.withStackTrace(throwable);
    }

    Failure fail = new Failure().withErrors(statusMessage);
    return fail.asMessage();
  }

}
