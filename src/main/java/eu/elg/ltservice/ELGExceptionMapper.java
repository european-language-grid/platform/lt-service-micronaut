/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import eu.elg.model.StatusMessage;

import java.util.function.Function;

/**
 * Class that maps an exception type to an ELG {@link StatusMessage}.  A standard set of these is registered by default
 * but you can add your own to handle other types of exceptions.
 *
 * @param <T> the exception type that this mapper handles
 */
public class ELGExceptionMapper<T extends Throwable> {

  private final Class<T> type;

  private final Function<T, StatusMessage> mapper;

  /**
   * Create a mapper for the given exception type that uses the supplied function to perform the mapping.
   *
   * @param type   the exception type to map
   * @param mapper mapping function responsible for the transformation
   */
  public ELGExceptionMapper(Class<T> type, Function<T, StatusMessage> mapper) {
    this.type = type;
    this.mapper = mapper;
  }

  /**
   * @return the exception type that this mapper can handle
   */
  public Class<T> getType() {
    return type;
  }

  /**
   * Map an exception to a status message using the mapping function passed to the constructor.
   *
   * @param throwable the exception
   * @return the mapped {@link StatusMessage}
   */
  public StatusMessage toStatusMessage(T throwable) {
    return mapper.apply(throwable);
  }
}
