/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import io.micronaut.core.convert.ArgumentConversionContext;
import io.micronaut.core.convert.exceptions.ConversionErrorException;
import io.micronaut.core.serialize.exceptions.SerializationException;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.bind.binders.AnnotatedRequestArgumentBinder;
import io.micronaut.jackson.serialize.JacksonObjectSerializer;
import jakarta.inject.Singleton;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Optional;

/**
 * Argument binder that takes the string value from a "form field" (a multipart part without a <code>filename</code>)
 * and parses it as JSON to a target type.
 */
@Singleton
public class JsonFieldArgumentBinder implements AnnotatedRequestArgumentBinder<JsonField, Object> {
  private final JacksonObjectSerializer objectSerializer;

  public JsonFieldArgumentBinder(JacksonObjectSerializer objectSerializer) {
    this.objectSerializer = objectSerializer;
  }

  @Override
  public Class<JsonField> getAnnotationType() {
    return JsonField.class;
  }

  @Override
  public BindingResult<Object> bind(ArgumentConversionContext<Object> context, HttpRequest<?> source) {
    String parameterName = context.getAnnotationMetadata()
            .stringValue(JsonField.class)
            .orElse(context.getArgument().getName());

    return new JsonFieldResult(parameterName, source, context.getArgument());
  }

  private class JsonFieldResult implements BindingResult<Object> {

    private Optional<Object> value;

    private final String parameterName;

    private final HttpRequest<?> source;

    private final Argument<Object> argument;

    JsonFieldResult(String parameterName, HttpRequest<?> source, Argument<Object> argument) {
      this.parameterName = parameterName;
      this.source = source;
      this.argument = argument;
    }

    @Override
    public Optional<Object> getValue() {
      //noinspection OptionalAssignedToNull
      if(value == null) {
        value = source.getBody().filter(b -> b instanceof Map).flatMap(b -> {
          Object rawValue = ((Map<?, ?>) b).get(parameterName);
          if(rawValue instanceof String) {
            rawValue = ((String) rawValue).getBytes(StandardCharsets.UTF_8);
          }
          if(rawValue instanceof byte[]) {
            try {
              return objectSerializer.deserialize((byte[]) rawValue, argument);
            } catch(SerializationException e) {
              if(e.getCause() instanceof Exception) {
                throw new ConversionErrorException(argument, (Exception) e.getCause());
              } else {
                throw e;
              }
            }
          } else {
            return Optional.empty();
          }
        });
      }
      return value;
    }
  }
}
