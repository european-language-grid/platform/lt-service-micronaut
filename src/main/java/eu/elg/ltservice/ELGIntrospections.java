/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.ltservice;

import eu.elg.model.*;
import eu.elg.model.requests.*;
import eu.elg.model.responses.*;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.TypeHint;

/**
 * This class exists only for the benefit of the Micronaut annotation processor to cause it to generate compile-time
 * reflection-free introspections for all the various ELG request and response mapping classes.
 */
@Introspected(classes = {Request.class,
        TextRequest.class, StructuredTextRequest.class,
        Markup.class, StructuredTextRequest.Text.class, AnnotationObject.class,
        BinaryRequest.class, AudioRequest.class, ImageRequest.class,
        AudioRequest.Format.class, ImageRequest.Format.class,
        Response.class,
        StoredResponse.class, AnnotationsResponse.class,
        TextsResponse.class, TextsResponse.Text.class,
        AudioResponse.class, AudioResponse.Format.class,
        ClassificationResponse.class, ClassificationClass.class,
        Failure.class, Progress.class, StatusMessage.class, ResponseMessage.class,
})
@TypeHint(value = ResponseMessage.class, accessType = {
        TypeHint.AccessType.ALL_DECLARED_CONSTRUCTORS,
        TypeHint.AccessType.ALL_DECLARED_FIELDS,
        TypeHint.AccessType.ALL_PUBLIC_METHODS
})
class ELGIntrospections {
}
