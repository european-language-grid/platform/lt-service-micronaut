# Helper for Micronaut-based ELG services

This library is intended as a helper for developers working in Java and other JVM-based languages (Groovy, Kotlin, ...)
to build ELG-compliant LT services. It is based on the [Micronaut](https://micronaut.io) microservices framework, which
has a number of features that make it particularly well-suited to ELG services:

- it includes non-blocking HTTP server and client implementations based on Netty, which can handle a large number of
  requests in a small number of processing threads with asynchronous I/O
  and [reactive streams](https://www.reactive-streams.org)
- it makes little to no use of runtime reflection within the framework, relying instead on compile-time annotation
  processing to pre-compute introspection data, AOP advice, etc. This means fast startup and lower memory footprint
  compared to reflection-based frameworks like Spring
- for even faster startup and smaller footprint, applications can often be compiled ahead of time
  using [GraalVM's native-image tool](https://www.graalvm.org/native-image/). Container images built this way can start
  up in milliseconds rather than seconds.

## Usage

To create an ELG-compatible service using this library you will need to:

1. Generate a new Micronaut application (using Micronaut version 3.5.2 or later), either using the [`mn` command line tool](https://micronaut.io/download/) or by
   generating a template application via [the "Micronaut launch" tool](https://micronaut.io/launch). If you intend to
   develop your code in a reactive style you will need to include the "feature" for your preferred reactive library,
   either [Project Reactor](https://projectreactor.io) (preferred - this is the library that Micronaut and the LT
   service helper use internally) or [RxJava3](https://github.com/ReactiveX/RxJava).
2. Add a dependency in your `build.gradle` or `pom.xml` to this library

  ```groovy
  implementation("eu.european-language-grid:lt-service-micronaut:1.1.0")
  ```

  ```xml
<dependency>
    <groupId>eu.european-language-grid</groupId>
    <artifactId>lt-service-micronaut</artifactId>
    <version>1.1.0</version>
</dependency> 
  ```

  _Release_ versions are published to the Central repository, but if you want to use a snapshot version of
  this helper you will need to add the appropriate repository to your Gradle/Maven configuration

  ```groovy
repositories {
  mavenCentral()
  maven { url "https://gitlab.com/api/v4/groups/european-language-grid/-/packages/maven/" }
}
  ```

  ```xml
<repositories>
  <repository>
    <id>elg-gitlab</id>
    <name>ELG Group GitLab Repo</name>
    <url>https://gitlab.com/api/v4/groups/european-language-grid/-/packages/maven/</url>
    <layout>default</layout>
    <releases><enabled>false</enabled></releases>
    <snapshots><enabled>true</enabled></snapshots>
  </repository>
</repositories>
  ```

3. Create a controller that extends `LTService` (for services that process text-based requests) or `BinaryLTService` (
   for services that process requests with binary content such as audio) and implement the relevant `handle`
   or `handleSync` method
4. Package your application as a Docker image and submit it to the ELG in the normal way.

## Using the `LTService` base classes

The `lt-service-micronaut` library is based around a pair of (effectively) abstract base classes that can be extended to
implement ELG-compliant endpoints. If you want to process `"text"` or `"structuredText"` requests then your controller
should directly extend `LTService`, for `"audio"` or `"image"` requests extend `BinaryLTService`.

### `TextRequest` example

```java
package eu.elg.example;

import eu.elg.ltservice.LTService;
import eu.elg.model.AnnotationObject;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;
import eu.elg.model.util.TextOffsetsHelper;
import io.micronaut.http.annotation.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller("/process")
public class ProcessController extends LTService<TextRequest, LTService.Context> {

  private static final Pattern PATTERN = Pattern.compile("(?i)penguin");

  // return type can be any subclass of Response<?>, or just use Response<?>
  // directly if your method can return different types in different cases
  @Override
  protected AnnotationsResponse handleSync(TextRequest request, Context ctx) {
    List<AnnotationObject> anns = new ArrayList<>();
    String content = request.getContent();
    // Helper to convert between Java String indices and ELG API offsets (which are
    // different if the content includes supplementary characters such as Emoji).
    TextOffsetsHelper helper = new TextOffsetsHelper(content);
    Matcher m = PATTERN.matcher(content);
    double progress = 0.0;
    while(m.find()) {
      anns.add(helper.annotationWithOffsets(m.start(), m.end()).withFeature("bird", true));
    }
    return new AnnotationsResponse().withAnnotations("Penguin", anns);
  }
}
```

The `@Controller` annotation defines the URL path at which your endpoint will listen for HTTP requests - `/process` is a
common path to use, but you can choose any path apart from `/elg-alive` (which is reserved for the default liveness
endpoint).

The `LTService` class has two type parameters, the first is the Java class from the `elg-java-bindings` library that
represents the type of request you want to handle - typically `TextRequest` - and the second should generally be set
to `LTService.Context` (see below for the reason why this is parameterized at all).

The actual processing logic happens in one of two `protected` methods, which your subclass must override.
The `handleSync` method shown above is the simplest if your processing is CPU-bound or requires the use of _blocking_
I/O - processing happens on a separate thread pool. If your processing can make use of _non-blocking_ I/O then you
should instead override `handle`, which returns a single-valued reactive streams `Publisher` (a `Mono` in Reactor
terminology). For example, if your service is a proxy to a remote endpoint you can use the Micronaut `HttpClient` which
is reactive in nature:

```java
@Controller("/proxy")
public class ProxyController extends LTService<TextRequest, LTService.Context> {

  private ReactorHttpClient client;

  public ProxyController(@Client ReactorHttpClient client) {
    this.client = client;
  }
  
  @Override
  protected Mono<TextsResponse> handle(TextRequest request, LTService.Context ctx) {
    return client.retrieve(
                    HttpRequest.POST("https://ltservice.example.com", request.getContent()),
                    MyServiceResponse.class)
            .single() // retrieve returns Flux but we expect one item
            .map(response -> toELG(response));
  }

  private TextsResponse toELG(MyServiceResponse r) { /* ... */ }
}
```

### Binary requests

For binary request types the principle is exactly the same, but for these requests the actual request content is
supplied separately from the `AudioRequest` or `ImageRequest` metadata. For these requests your controller should
extend `BinaryLTService`, which takes _three_ type parameters:

```java
import io.micronaut.http.multipart.StreamingFileUpload;

@Controller("/transcribe")
public class ASRController extends
        BinaryLTService<AudioRequest, StreamingFileUpload, LTService.Context> {

  @Override
  protected Mono<TextsResponse> handle(AudioRequest request, StreamingFileUpload content, LTService.Context ctx) {
    // implementation goes here
  }
}
```

The extra parameter is the type to which the binary content should be bound, and should be one of three types:

- `StreamingFileUpload` - this is the preferred type as it allows the request data to be handled in a streaming fashion
  without full buffering, but can only be used in a reactive streams `handle` style. It acts as a `Publisher` that
  yields the data as a stream of one _or more_ chunks.
- `CompletedFileUpload` - ensures the file upload is complete and possibly buffered to a temporary file before your
  handler is called. This type can be processed using either `handle` or `handleSync`
- `byte[]` - gathers all the data into a single in-memory buffer and passes that to your handler function. This is the
  simplest type to work with but the least memory-efficient option.

## Errors

The return type of the `handle` or `handleSync` method is for _successful_ responses.  In the case of failures `handleSync` should throw an exception, and `handle` should return a failing publisher (one that emits an error signal instead of a `Response` value).  By default an exception generated by a handler method will be returned to the caller as an ELG-standard `"failure"` message with code `elg.service.internalError` and the exception message included in the message parameters.  If you want more control over the precise ELG error code, or you want to return a failure with more than one message in its `errors` array then you should use the `ELGException` type which provides full flexibility in this regard.

## Building and deployment

The Micronaut Gradle plugin has built in support for packaging a Micronaut application as a Docker image:

```
./gradlew dockerBuild
```

however we recommend the use of the [Jib](https://github.com/GoogleContainerTools/jib) tool as it will build images that make better use of space in container registries.  To use this, add the following to the `plugins` section at the top of your `build.gradle`:

```groovy
  id 'com.google.cloud.tools.jib' version '3.1.4'
```

Now you can build an image in your local Docker daemon using `./gradlew jibDockerBuild`, or to push the image to a remote repository use `./gradlew jib -Djib.to.image=registry.example.com/my-service:latest`.  By default Jib will use the credentials from your local Docker configuration, or you can supply them directly in the build file.

Alternatively, Micronaut supports compiling apps to native code using GraalVM via `./gradlew dockerBuildNative` (to build the image in your local Docker) and `./gradlew dockerPushNative` (to push the image to a remote registry).  See the [Micronaut documentation](https://docs.micronaut.io/latest/guide/#graalServices) for more details.  Not all applications are suitable for this treatment, in particular native images have strict restrictions on their use of Java reflection to load classes, but if your app is compatible then native compilation will produce the smallest image size and fastest startup time, which is highly desirable for ELG services.

# Advanced topics

## Progress reporting

The ELG API specification allows long-running LT services to return "progress" messages during processing to inform
their caller of the percentage completion (if known) and any information messages generated along the way ("finished
finding entities", "extracted relations", etc.). This is supported through the `LTService.Context` parameter which is
passed to all the handler methods - handler code can call `ctx.reportProgress` to send a progress update. The helper
also continually re-emits the most recent progress message if no further updates have been produced for a set amount of
time (by default every 15 seconds, but this is overridable by a configuration property), as a way of helping to ensure
that the HTTP connection remains alive and does not drop due to timeout. The `reportProgress` method takes a percentage
which should be between 0.0 and 100.0, and optionally a `StatusMessage` with more information.

## Temporary storage

The ELG platform provides a "temporary storage" service where LT services can send arbitrary data files, and receive in response a URL that the caller of the service will be able to access from outside the platform in order to download the data that was stored.  This library provides a Java interface to the temporary storage service, which is available as a bean of type `TemporaryStorage` for injection into your `LTService` controller.  The `TemporaryStorage` helper method returns a reactive streams `Publisher`, there is a convenience sub-interface `ReactorTemporaryStorage` that returns the concrete Project Reactor `Mono` type instead.

```java
import eu.elg.ltservice.LTService;
import eu.elg.ltservice.ReactorTemporaryStorage;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;
import io.micronaut.http.MediaType;
import reactor.core.publisher.Mono;

@Controller("/parsetree")
public class ParseController extends LTService<TextRequest, LTService.Context> {

  private Parser parser;
  private ReactorTemporaryStorage storage;

  public ParseController(ReactorTemporaryStorage storage, Parser parser) {
    this.storage = storage;
    this.parser = parser;
  }

  protected Mono<AnnotationsResponse> handle(TextRequest req, LTService.Context ctx) {
    // assume parser.parse takes text and returns a result including some annotations
    // and a byte[] PNG image of the parse structure
    return Mono.fromSupplier(() -> parser.parse(req.getContent()))
      // it takes some time to run, so run it on the syncScheduler so as not to
      // block the event loop
      .subscribeOn(syncScheduler())
      .flatMap(parseResult ->
        // send the PNG to the storage service, non-blocking
        storage.store(parseResult.getPng(), MediaType.IMAGE_PNG_TYPE)
                // and include the resulting URI in response
                .map(uri -> new AnnotationsResponse()
                      .withAnnotations(parseResult.toAnnotations())
                      .withFeature("treeImage", uri)));
  }
}
```

## Advanced context usage

Normally when you are writing your own Micronaut controller action methods you can declare additional parameters to bind
things like path parameters from the URI. For example if you wanted to write a single controller that can handle many MT
language pairs you might choose to map it as

```java
@Controller("/translate/{from}/{to}")
public class TranslateController ...
```

and then bind the `from` and `to` parameters in your controller action using `@PathVariable`. But when you are using
the `LTService` base classes this is not possible, as the actual _actions_ are methods defined in the base class. This
is the reason why the base classes take a type parameter for the context class - in order to bind path variables you
must create a _subclass_ of `LTService.Context` and pass your subclass as the type parameter:

```java
@Controller("/translate/{from}/{to}")
public class TranslationService extends LTService<TextRequest,
        TranslationService.Context> { // this is the Context class below

  @Introspected
  public static class Context extends LTService.Context {
    public final String from;
    public final String to;

    public Context(@PathVariable("from") String from,
                   @PathVariable("to") String to) {
      this.from = from;
      this.to = to;
    }

  }

  // Bean that does the actual work, left as an exercise...
  private Translator translator;

  // Dependency injection via constructor arguments
  public TranslationService(Translator translator) {
    this.translator = translator;
  }

  protected AnnotationsResponse handleSync(TextRequest req, 
       TranslationService.Context ctx) throws ELGException {
    // ctx.from and ctx.to are bound to the URL path parameters,
    // so a call to /translate/en/de would set ctx.from = "en"
    // and ctx.to = "de"
    return translator.translate(ctx.from, ctx.to, req.getContent());
  }
}
```

**Note** the `@Introspected` annotation is _required_ on your `Context` subclass.
